package ru.polesov.javacore.homework;

import java.util.Comparator;

public class App {
    public static void main(String[] args) {

        MyList<CastCost> list = new MyList<>();

        //test list
        list.add(new CastCost("Газированная вода", 20));
        list.add(new CastCost("Пирожки", 30.5));
        list.add(new CastCost("Еда", 16560.90));
        list.add(new CastCost("Бензин", 1495.45));
        list.add(new CastCost("Налоги", 6560.90));
        list.add(new CastCost("Одежда", 1560.80));
        list.add(new CastCost("Не понятно куда ушли", 7560.90));

        //get
        var opt = list.get(26);
        if (opt.isPresent()) {
            System.out.println(opt.get());
        } else {
            System.out.println("get null");
        }

        //contains
        System.out.println(list.contains(null
        ));

        //empty
        System.out.println(list.isEmpty());

        //last
        System.out.println(list.last(new CastCost("Пирожки", 30.5)));

        //first
        System.out.println(list.first(new CastCost("Газированная вода", 20)));

        //insert
        try {
            list.insert(3, new CastCost("ЖКХ", 7450));
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(list.get(3));

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        //remove
        System.out.println(list.size());
        try {
            list.remove(15);
        } catch (Exception e) {
            System.out.println(e);
        }

        // addAll
        MyList<CastCost> newList = new MyList<>();
        for (int i = 0; i < 15; i++) {
            if (i != 10) {
                newList.add(new CastCost("Мелкие траты", i));
            } else {
                newList.add(null);
            }
        }
        list.addAll(newList);
        System.out.println(list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).orElse(null));
        }

        //sort
        Comparator<CastCost> comparator = (o1, o2) -> {
            if (o1 == null || o2 == null) return 0;
            if (o1.getCost() == o2.getCost()) {
                return 0;
            }
            return (o1.getCost() > o2.getCost()) ? 1 : -1;
        };

        System.out.println("Сортированный список: ");
        var sortList = list.sort(comparator);
        for (int i = 0; i < sortList.size(); i++) {
            System.out.println(sortList.get(i));
        }

        System.out.println("Изначальный список: ");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        //shuffle
        System.out.println("Перемешанный список: ");
        var shuffleList = list.shuffle();
        for (int i = 0; i < shuffleList.size(); i++) {
            System.out.println(shuffleList.get(i));
        }

        System.out.println("Изначальный список: ");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
