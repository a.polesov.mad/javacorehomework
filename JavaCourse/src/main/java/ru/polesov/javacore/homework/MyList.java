package ru.polesov.javacore.homework;

import java.lang.reflect.Array;
import java.util.*;

public class MyList<T> implements AdvancedList<T>, AuthorHolder {

    private T[] items;
    private int count;
    private final int increaseSize = 10;

    MyList() {
        this.count = 0;
        this.items = (T[]) Array.newInstance(Object.class, increaseSize);
    }

    @Override
    public AdvancedList<T> shuffle() {
        MyList<T> shuffleList = new MyList<>();
        Random random = new Random();
        shuffleList.addAll(this);
        int newIndexToSwap;
        int sizeList = shuffleList.size() - 1;
        for (int i = 0; i < sizeList; i++) {
            newIndexToSwap = random.nextInt(sizeList);
            shuffleList.swap(i, newIndexToSwap);
        }
        return shuffleList;
    }

    @Override
    public AdvancedList<T> sort(Comparator<T> comparator) {
        MyList<T> sortedList = new MyList<>();
        sortedList.addAll(this);
        sortedList.quickSort(0, sortedList.size() - 1, comparator);
        return sortedList;
    }

    @Override
    public String author() {
        return "Alexey Polesov";
    }

    @Override
    public void add(T item) {
        if (size() == (items.length - 1)) {
            increaseArray(items.length + increaseSize);
        }
        items[count] = item;
        count++;
    }

    @Override
    public void insert(int index, T item) throws Exception {
        if ((index >= 0) && (index < size())) {
            items[index] = item;
        } else {
            throw new Exception("There is no element with an index " + index + " in the list");
        }
    }

    @Override
    public void remove(int index) throws Exception {
        if ((index >= 0) && (index < size())) {
            T[] modifiedList = (T[]) Array.newInstance(Object.class, items.length);
            int j = 0;
            for (int i = 0; i < items.length; i++) {
                if (i != index) {
                    modifiedList[j] = items[i];
                    j++;
                }
            }
            items = modifiedList;
            count--;
        } else {
            throw new Exception("There is no element with an index " + index + " in the list");
        }
    }

    @Override
    public Optional<T> get(int index) {
        if (index < size()) {
            return Optional.ofNullable(items[index]);
        }
        return Optional.empty();
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void addAll(SimpleList<T> list) {
        if (list != null) {
            if ((size() + list.size()) > items.length) {
                increaseArray(items.length + list.size());
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isPresent()) {
                    add(list.get(i).get());
                } else {
                    add(null);
                }
            }
        }
    }

    @Override
    public int first(T item) {
        for (int i = 0; i < size(); i++) {
            Optional<T> itemList = this.get(i);
            if (itemList.isPresent()) {
                if (itemList.get().equals(item)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int last(T item) {
        for (int i = size() - 1; i >= 0; i--) {
            Optional<T> itemList = this.get(i);
            if (itemList.isPresent()) {
                if (itemList.get().equals(item)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T item) {
        for (int i = 0; i < size(); i++) {
            Optional<T> itemList = this.get(i);
            if (itemList.isPresent()) {
                if (itemList.get().equals(item)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    private void swap(int i, int j) {
        T temp = this.items[i];
        this.items[i] = this.items[j];
        this.items[j] = temp;
    }

    private void quickSort(int initialLimit, int ultimateLimit, Comparator<T> comparator) {

        if (this.size() == 0)
            return;

        if (initialLimit >= ultimateLimit)
            return;

        int middle = initialLimit + (ultimateLimit - initialLimit) / 2;
        T middleItem = this.items[middle];

        int i = initialLimit, j = ultimateLimit;
        while (i <= j) {

            while (comparator.compare(this.items[i], middleItem) < 0) {
                i++;
            }

            while (comparator.compare(this.items[j], middleItem) > 0) {
                j--;
            }

            if (i <= j) {
                this.swap(i, j);
                i++;
                j--;
            }
        }
        if (initialLimit < j)
            quickSort(initialLimit, j, comparator);

        if (ultimateLimit > i)
            quickSort(i, ultimateLimit, comparator);
    }

    private void increaseArray(int newSize) {
        items = Arrays.copyOf(items, newSize);
    }
}
