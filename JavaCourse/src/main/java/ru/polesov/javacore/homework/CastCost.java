package ru.polesov.javacore.homework;

import java.util.Objects;

public class CastCost {

    private final String name;

    public double getCost() {
        return cost;
    }

    private double cost;

    CastCost(String name, double cost) {
        this.name = name;
        this.cost = cost;

    }

    public String toString() {
        return "CastCost " + name + " " + cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CastCost castCost = (CastCost) o;
        return Objects.equals(name, castCost.name) && Objects.equals(cost, castCost.cost);
    }
}
