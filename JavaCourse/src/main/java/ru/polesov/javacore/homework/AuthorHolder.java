package ru.polesov.javacore.homework;

public interface AuthorHolder {
    String author();
}